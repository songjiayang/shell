
// promiseSleep returns a Promise after an amount of milliseconds
export function promiseSleep (milliseconds, value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value)
    }, milliseconds)
  })
}
