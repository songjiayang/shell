import { dialog, shell } from 'electron'
import { autoUpdater } from 'electron-updater'
import pjson from '../../../package.json'
import log from 'electron-log'

autoUpdater.autoInstallOnAppQuit = false
autoUpdater.autoDownload = false

autoUpdater.on('update-available', (info) => {
  log.info('An update is available')
  const btnId = dialog.showMessageBox({
    type: 'info',
    message: `A newer version is available!`,
    detail: `${pjson.productName} ${info.version} is ready for download`,
    buttons: ['Remind me later', 'Download now'],
    cancelId: 0,
    defaultId: 1
  })
  const downloadURL = info.path ? `https://get.siderus.io/orion/${info.path}` : pjson.homepage
  if (btnId === 1) {
    shell.openExternal(downloadURL)
  }
})

autoUpdater.on('checking-for-update', () => {
  log.info('Checking for updates...')
})

export function checkForUpdates () {
  return autoUpdater.checkForUpdates()
}

export default autoUpdater
