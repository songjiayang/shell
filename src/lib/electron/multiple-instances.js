import log from 'electron-log'

export function blockMultipleInstances (app) {
  let gotTheLock = app.requestSingleInstanceLock()
  if (!gotTheLock) {
    log.warn('[App] Single instance lock detected. Quitting')
    app.quit()
  }
}

export default blockMultipleInstances
