import React from 'react'
import ReactDom from 'react-dom'
import { ipcRenderer } from 'electron'
import styled from 'styled-components'

import ProgressBar from './ProgressBar'
import OrionLogo from '../../../assets/logo-white.svg'

const Window = styled.div`
  font: caption;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 24px;
  color: white;
`
const Orion = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 30px;
  -webkit-app-region: drag;
`

const Progress = styled.div`
  font-size: 1.10em;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  -webkit-app-region: drag;
`

const SubHeader = styled.div`
  font-size: 0.90em;
`

const UseLocalUIButton = styled.div`
  margin: 12px 12px 0px 12px;
`

class LoadingWindow extends React.Component {
  state = {
    text: 'Loading',
    percentage: 0,
    skipLatestUIFetch: false
  }

  useLocalUI = () => {
    this.setState({ skipLatestUIFetch: true })
    ipcRenderer.send('message', {
      method: 'shell/useLocalUI'
    })
  }

  componentDidMount () {
    ipcRenderer.on('set-progress', (event, data) => this.setState(data))
  }

  componentWillUnmount () {
    ipcRenderer.removeAllListeners('set-progress')
  }

  render () {
    const {
      skipLatestUIFetch,
      percentage, text
    } = this.state

    return (
      <Window>
        <Orion>
          <OrionLogo height='150px' color="white" />
        </Orion>

        <Progress>
          {text || 'Loading...'}
          <ProgressBar percentage={percentage} />
        </Progress>
        <SubHeader>
          Please, ensure that ports 4001 and 5001 are not blocked by the firewall
        </SubHeader>
        <UseLocalUIButton>
          { !skipLatestUIFetch && percentage <= 50 &&
            <button disabled={skipLatestUIFetch} onClick={this.useLocalUI}>
              Skip updates checks
            </button>
          }
        </UseLocalUIButton>
      </Window>
    )
  }
}

ReactDom.render(<LoadingWindow />, document.querySelector('#host'))
