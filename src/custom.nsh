!macro customInstall
      ; Writing the Windows Context menu in the registry. This will add right
      ; click capabiliteis. HKCU = Current user, HKLM = Local Machine, all users

      ; Adding it for any file
      WriteRegStr HKLM "Software\Classes\*\shell\Siderus Orion\command" "" `"$INSTDIR\Siderus Orion.exe" --add "%1"`
      WriteRegStr HKLM "Software\Classes\*\shell\Siderus Orion" "Icon" "$INSTDIR/Siderus Orion.exe"
      WriteRegStr HKLM "Software\Classes\*\shell\Siderus Orion" "" "Add to Siderus Orion"

      ; Adding the option for Directories
      WriteRegStr HKLM "Software\Classes\Directory\shell\Siderus Orion\command" "" `"$INSTDIR\Siderus Orion.exe" --add "%1"`
      WriteRegStr HKLM "Software\Classes\Directory\shell\Siderus Orion" "Icon" "$INSTDIR/Siderus Orion.exe"
      WriteRegStr HKLM "Software\Classes\Directory\shell\Siderus Orion" "" "Add to Siderus Orion"

      ; Adding Options for background Directories
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Siderus Orion\command" "" `"$INSTDIR\Siderus Orion.exe" --add "%V"`
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Siderus Orion" "Icon" "$INSTDIR/Siderus Orion.exe"
      ; WriteRegStr HKLM "Software\Classes\Directory\background\shell\Siderus Orion" "" "Add to Siderus Orion"
!macroend

!macro customUnInstall
      ; This will delete the keys saved before for right-click capabilities
      DeleteRegKey HKLM "Software\Classes\*\shell\Siderus Orion"
      DeleteRegKey HKLM "Software\Classes\Directory\shell\Siderus Orion"
      DeleteRegKey HKLM "Software\Classes\Directory\background\shell\Siderus Orion"
      ; Delete the AppData directory
      ; RMDir /r "$APPDATA\${APP_FILENAME}"
!macroend