# Changelog
All notable changes to this project will be documented in this file.
To download the latest version of Orion, please visit:
[https://orion.siderus.io/](https://orion.siderus.io/)

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.1.3] - 2019-02-16
### Added
- New Cards feature, providing quick info about the peer to the user
- Added option to disable the cards in the settings
- Added quick help cards to give tips and tricks to the user
- New Network Panel for connectivity
- Adds new Known Peers list to keep friend-to-friend networks
- Monitor peers to let the user know which one of them is connected
- Adds buttons to Ping peers
- Double click on a file will open the properties
- Activities now are clickable
- Importing a file will now show a progress bar in the Activities
- It is now possible to resolve IPNS and import them (equivalent of `ipfs pin add /ipns/xyz`)
- Adds "Copy public link" to Files menu
### Fixed
- App Menu on macOS will not report an error when not selecting a file
- Loading single-file objects now will not reload the links
- Fullscreen on macOS now has a proper menu icon location
- The size of some elements is updated to not change when changing pages
### Changed
- Upgraded to Electron 4
- File lists now supports pagination, for Peers handling more than 500 objects
- Upgrade libraries and dependencies to latest supported version
- Cache objects are including only metadata, improving performances and memory usage
- Uses Matomo instead of GA and Mixpanel to respect users privacy
- Connectivity messages are handled by a dedicated worker
- Copy public Link from Properties will copy the hash + path
- Terminology: uses Peer instead of Node in the text
- Terminology: uses the word Telemetry properly
### Removed
- Settings page doesn't show anymore the peer addresses (moved to Network)
- Settings page doesn't provide a button to add bootstrap nodes (moved to Network)

## [v1.0.12] - 2019-01-27
### Added
- QRCode in Properties
### Fixed
- Properties are now loading correctly when loaded for the first time
- IE users are getting an alert that suggests to upgrade to Firefox, Safari or Chrome

## [v1.0.11-beta] - 2019-01-25
### Added
- Check for Internet Explorer and Microsoft Edge
### Fixed
- Multiaddress invalid input bug
- Multihash invalid input bug
- IPNS key selection when publishign ([g221](https://github.com/Siderus/Orion/issues/221))

## [v1.0.10] - 2019-01-18
### Fixed
- Fixes token to report anonymously bug and usage

## [v1.0.9] - 2019-01-14
### Added
- First stable release

## [v1.0.8-beta] - 2019-01-08
### Added
- Support for AppImage for any GNU/Linux distribution
- Versio check on UI
- UI is loaded from IPNS (DNSLINK) by channel and Minor version (ex 1.0.X vs 1.1.X)
### Changed
- Checks for support for Workers from Browsers
- Updated HTTP API libraries
- New logo and colors
- Loading Window font fixes with multiple platforms
- Updated some dependencies due to security fixes
### Fixed
- Adding files will report the right progress
- Fixed problem when adding files or Directories from UI
- Fixed bug when the user cancel adding a file
### Removed
- Old dependencies not used

## [v1.0.6-beta] - 2018-12-25
### Added
 - When an error occurs the user can change the API endpoint (QuickFix)
 - The About page now shows the App and UI versions
 - Each page loads instantly and is pre-loaded at the beginning
 - Support for Offline usage
 - Including the UI in the Standalone App for fallback
 - Added Search form in File view
 - Better descriptions of Settings options
### Changed
 - Messaging between UI and Worker is using less memory (deletes used requests)
 - The events are loaded via an Interval and not a Timeout
 - Increased the waiting time for IPFS to 5 seconds
 - Improved error reporting when API are failing
### Fixed
 - Loading the UI will wait 2 extra seconds for the Worker to start
 - Fixed bug with IPNS taking too much time loading data causing timeout
 - Fixed bug on first run when the repository was not initialised correctly
 - Fixed null values in Settings page

## [v1.0.5-beta] - 2018-12-19
### Added
- Ability to customize the Gateway to use
- Current Usage of Bandwidth and Peers count (Settings)
- Better usage/feedback tracking
- Added internal store for cross-components data
- Automatic cleaning of electron cache to prevent error with workers (same name of the file)
- Loading page when slow fetching the Interface
- Added out-of-the-box PubSub support
### Changed
- Updated Siderus Logo
- Fixed Bug when adding a file and having multiple instances running causing the window to close
- Changed the Loading interface
- Changed the way data is stored to improve the application performances
- Introduced Typescript in some UI components
- Changed the Activity type (status property, completed/finished deprecated)
- Uses ipfs.dns instead of ipfs.name.resolve to speed up name resolution
### Removed
- Removed waitingForWorker on components in favor of App-wide loading interface

## [v1.0.4-beta] - 2018-12-10
### Added
- New Windows feature to add file directly from Explorer (Right click on files and directories)
- New support for `--add` when used with CLI
- Frameless look on Windows and Linux too
- Faster loading of the SPA / Main Interface
### Changed
- Fixed typos
- Upgraded dependencies due to security fixes
